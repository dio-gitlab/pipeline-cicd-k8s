# Passo a Passo

### Criar arquivo .gitlab-ci.yml
 - Usar dados das senhas nas variaves e configurar no Gitlab
> NOME_DO_PROJETO > SETTINGS > CI/CD > VARIABLES > ADD VARIABLES


### Criar Cluster no Google Cloud Platform com 3 nodes
 - Conecte-se ao cluster Kubernetes via gcloud CLI

### Criar VM Bastion no GCP e configura-lo previamente com os dados do GCP
    - Chave Publica e Chave Privada
    - Para Windows (Puty e Puttygen)
    - Install gcloud
    - Configurar usuario gcloud (gcloud config set account carlav.danelli@gmail.com)
    - Iniciar gcloud init
    - gcloud auth login --no-launch-browser
    - sudo apt-get update
    - sudo apt-get install git 
    - sudo apt-get install kubectl
    - sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin
    - Diretorio padrao (/home/gcp) ou ($PWD)
    
### Finalizar a configuração do arquivo .gitlab-ci.yml
    - Configurar senha de acesso ssh do gcp 
    - (converter .ppk para .pem usando puttygen) e colocar hash no Gitlab do tipo "file"
    - Adicioanr uma nova variavel de SSH do servidor Bastion com o IP publico do mesmo. 

### Importar secrets para o Cluster

### Inserir ip publico do Loadbalancer do arquivo Javascript

### Liberar regras de firewall se necessario

### Rodar Pipeline
